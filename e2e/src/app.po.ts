import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> { // tslint:disable-line
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTitleText(): Promise<string> { // tslint:disable-line
    return element(by.css('app-root .content span')).getText() as Promise<string>;
  }
}
