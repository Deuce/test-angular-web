import { Action } from '@ngrx/store';

export enum logInActionsTypes {
  LOGIN_EMAIL_CHANGED = 'LOGIN_EMAIL_CHANGED',
  LOGIN_PASSWORD_CHANGED = 'LOGIN_PASSWORD_CHANGED',
  LOGIN_SET_VALIDITY = 'LOGIN_SET_VALIDITY',
}

export class LogInEmailChanged implements Action {
  readonly type = logInActionsTypes.LOGIN_EMAIL_CHANGED;
  constructor(public payload: { userEmail: string }) {}
}

export class LogInPasswordChanged implements Action {
  readonly type = logInActionsTypes.LOGIN_PASSWORD_CHANGED;
  constructor(public payload: { userPassword: string }) {}
}

export class LogInSetValidity implements Action {
  readonly type = logInActionsTypes.LOGIN_SET_VALIDITY;
  constructor(public payload: { isValid: boolean }) {}
}

export type All = LogInEmailChanged | LogInPasswordChanged | LogInSetValidity;
