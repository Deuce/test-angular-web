import { Action } from '@ngrx/store';

export enum chatSettingsActionType {
  ADD_PHRASE = 'ADD_PHRASE',
  DELETE_PHRASE = 'DELETE_PHRASE',
  CHANGE_GREETING = 'CHANGE_GREETING',
}

export class AddPhrase implements Action {
  readonly type = chatSettingsActionType.ADD_PHRASE;
  constructor(
    public payload: {
      phrase: string;
    },
  ) {}
}

export class DeletePhrase implements Action {
  readonly type = chatSettingsActionType.DELETE_PHRASE;
  constructor(public payload: string) {}
}

export class ChangeGreeting implements Action {
  readonly type = chatSettingsActionType.CHANGE_GREETING;
  constructor(
    public payload: {
      greeting: string;
    },
  ) {}
}

export type All = AddPhrase | DeletePhrase | ChangeGreeting;
