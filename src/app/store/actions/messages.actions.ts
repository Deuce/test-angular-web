import { Action } from '@ngrx/store';

export enum messagesActionType {
  MESSAGES_LOADING = '[Mess] LOADING',
  MESSAGES_LOADING_SUCCESS = '[Mess] LOADING_SUCCESS',
  ADD_MESSAGE = '[Mess] ADD_MESSAGE',
}

export class Loading implements Action {
  readonly type = messagesActionType.MESSAGES_LOADING;
  constructor(public payload: object) {}
}

export class LoadingSuccess implements Action {
  readonly type = messagesActionType.MESSAGES_LOADING_SUCCESS;
  constructor(
    public payload: {
      messages: Array<object>;
    },
  ) {}
}

export class AddMessage implements Action {
  readonly type = messagesActionType.ADD_MESSAGE;
  constructor(
    public payload: {
      message: object;
    },
  ) {}
}

export type All = Loading | LoadingSuccess | AddMessage;
