/* tslint:disable */
import { Action } from '@ngrx/store';

export enum conversationsActionTypes {
  CONVERSATIONS_LOADING = '[Conv] LOADING',
  CONVERSATIONS_LOADING_SUCCESS = '[Conv] LOADING_SUCCESS',
}

export class Loading implements Action {
  readonly type = conversationsActionTypes.CONVERSATIONS_LOADING;
  constructor(public payload: any) {}
}

export class LoadingSuccess implements Action {
  readonly type = conversationsActionTypes.CONVERSATIONS_LOADING_SUCCESS;
  constructor(
    public payload: {
      allConversations: Array<object>;
      pendingConversations: Array<object>;
      activeConversations: Array<object>;
      endedConversations: Array<object>;
      savedConversations: Array<object>;
    },
  ) {}
}

export type All = Loading | LoadingSuccess;
