/* tslint:disable */
import { Action } from '@ngrx/store';
import { User } from '../../models';

export enum authActionType {
  LOGIN = '[Auth] LOGIN',
  LOGIN_SUCCESS = '[Auth] LOGIN_SUCCESS',
  LOGIN_FAILURE = '[Auth] LOGIN_FAILURE',
  SIGNUP = '[Auth] SIGNUP',
  SIGNUP_SUCCESS = '[Auth] SIGNUP_SUCCESS',
  SIGNUP_FAILURE = '[Auth] SIGNUP_FAILURE',
  CHANGEPASS = '[Auth] CHANGEPASS',
  CHANGEPASS_SUCCESS = '[Auth] CHANGEPASS_SUCCESS',
  CHANGEPASS_FAILURE = '[Auth] CHANGEPASS_FAILURE',
  LOGOUT = '[Auth] lOGOUT',
}

export class LogIn implements Action {
  readonly type = authActionType.LOGIN;
  constructor(public payload: User) {}
}

export class LogInSuccess implements Action {
  readonly type = authActionType.LOGIN_SUCCESS;
  constructor(public payload: User) {}
}

export class LogInFailure implements Action {
  readonly type = authActionType.LOGIN_FAILURE;
  constructor(public payload: object) {}
}

export class SignUp implements Action {
  readonly type = authActionType.SIGNUP;
  constructor(public payload: User) {}
}

export class SignUpSuccess implements Action {
  readonly type = authActionType.SIGNUP_SUCCESS;
  constructor(public payload: User) {}
}

export class SignUpFailure implements Action {
  readonly type = authActionType.SIGNUP_FAILURE;
  constructor(public payload: object) {}
}

export class ChangePass implements Action {
  readonly type = authActionType.CHANGEPASS;
  constructor(public payload: any) {}
}

export class ChangePassSuccess implements Action {
  readonly type = authActionType.CHANGEPASS_SUCCESS;
  constructor(public payload: any) {}
}

export class ChangePassFailure implements Action {
  readonly type = authActionType.CHANGEPASS_FAILURE;
  constructor(public payload: object) {}
}

export class LogOut implements Action {
  readonly type = authActionType.LOGOUT;
}

export type All =
  | LogIn
  | LogInSuccess
  | LogInFailure
  | SignUp
  | SignUpSuccess
  | SignUpFailure
  | ChangePass
  | ChangePassSuccess
  | ChangePassFailure
  | LogOut;
