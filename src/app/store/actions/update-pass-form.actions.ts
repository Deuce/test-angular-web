import { Action } from '@ngrx/store';

export enum updatePassActionTypes {
  UPDATEPASS_PASSWORD_CHANGE = 'UPDATEPASS_PASSWORD_CHANGE',
  UPDATEPASS_NEW_PASSWORD_CHANGE = 'UPDATEPASS_NEW_PASSWORD_CHANGE',
  UPDATEPASS_SET_VALIDITY = 'UPDATEPASS_SET_VALIDITY',
}

export class UpdatePassPasswordChanged implements Action {
  readonly type = updatePassActionTypes.UPDATEPASS_PASSWORD_CHANGE;
  constructor(public payload: { userPassword: string }) {}
}

export class UpdatePassNewPasswordChanged implements Action {
  readonly type = updatePassActionTypes.UPDATEPASS_NEW_PASSWORD_CHANGE;
  constructor(public payload: { userNewPassword: string }) {}
}

export class UpdatePassSetValidity implements Action {
  readonly type = updatePassActionTypes.UPDATEPASS_SET_VALIDITY;
  constructor(public payload: { isValid: boolean }) {}
}

export type All = UpdatePassPasswordChanged | UpdatePassNewPasswordChanged | UpdatePassSetValidity;
