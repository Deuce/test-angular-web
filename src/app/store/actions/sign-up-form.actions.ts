import { Action } from '@ngrx/store';

export enum signUpActionTypes {
  SIGNUP_EMAIL_CHANGED = 'SIGNUP_EMAIL_CHANGED',
  SIGNUP_PASSWORD_CHANGED = 'SIGNUP_PASSWORD_CHANGED',
  SIGNUP_CONFIRM_PASSWORD_CHANGED = 'SIGNUP_CONFIRM_PASSWORD_CHANGED',
  SIGNUP_SET_VALIDITY = 'SIGNUP_SET_VALIDITY',
}

export class SignUpEmailChanged implements Action {
  readonly type = signUpActionTypes.SIGNUP_EMAIL_CHANGED;
  constructor(public payload: { userEmail: string }) {}
}

export class SignUpPasswordChanged implements Action {
  readonly type = signUpActionTypes.SIGNUP_PASSWORD_CHANGED;
  constructor(public payload: { userPassword: string }) {}
}

export class SignUpConfirmPasswordChanged implements Action {
  readonly type = signUpActionTypes.SIGNUP_CONFIRM_PASSWORD_CHANGED;
  constructor(public payload: { userConfirmPassword: string }) {}
}

export class SignUpSetValidity implements Action {
  readonly type = signUpActionTypes.SIGNUP_SET_VALIDITY;
  constructor(public payload: { isValid: boolean }) {}
}

export type All = SignUpEmailChanged | SignUpPasswordChanged | SignUpConfirmPasswordChanged | SignUpSetValidity;
