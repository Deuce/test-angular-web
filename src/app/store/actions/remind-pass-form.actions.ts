import { Action } from '@ngrx/store';

export enum remindPassActionTypes {
  REMINDPASS_EMAIL_CHANGED = 'REMINDPASS_EMAIL_CHANGED',
  REMINDPASS_SET_VALIDITY = 'REMINDPASS_SET_VALIDITY',
}

export class RemindPassEmailChanged implements Action {
  readonly type = remindPassActionTypes.REMINDPASS_EMAIL_CHANGED;
  constructor(public payload: { userEmail: string }) {}
}

export class RemindPassSetValidity implements Action {
  readonly type = remindPassActionTypes.REMINDPASS_SET_VALIDITY;
  constructor(public payload: { isValid: boolean }) {}
}

export type All = RemindPassEmailChanged | RemindPassSetValidity;
