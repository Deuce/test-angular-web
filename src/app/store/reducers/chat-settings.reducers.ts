import { IPhrasesState, getDefaultPhrasesState } from './../state/chat-support-state';
import { All, chatSettingsActionType } from '../actions/chat-settings.actions';

export const phrasesReducer = (state: IPhrasesState = getDefaultPhrasesState(), action: All): IPhrasesState => {
  switch (action.type) {
    case chatSettingsActionType.ADD_PHRASE: {
      const phrase = action.payload.phrase;

      return {
        ...state,
        phrases: [...state.phrases, phrase],
      };
    }
    case chatSettingsActionType.DELETE_PHRASE: {
      return {
        ...state,
        phrases: state.phrases.filter((phrase: string) => phrase !== action.payload),
      };
    }
    case chatSettingsActionType.CHANGE_GREETING: {
      return {
        ...state,
        greeting: action.payload.greeting,
      };
    }
    default: {
      return state;
    }
  }
};
