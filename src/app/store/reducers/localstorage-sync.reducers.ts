/* tslint:disable */
import { ActionReducer, MetaReducer } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({ keys: ['auth', 'phras'], rehydrate: true })(reducer);
}

export const metaReducers: MetaReducer<any, any>[] = [localStorageSyncReducer];
