import { IConversationsState, getDefaultConversationsState } from './../state/chat-support-state';
import { All, conversationsActionTypes } from '../actions/conversations.actions';

export const conversationsReducer = (
  state: IConversationsState = getDefaultConversationsState(),
  action: All,
): IConversationsState => {
  switch (action.type) {
    case conversationsActionTypes.CONVERSATIONS_LOADING: {
      return {
        ...state,
        allConversations: [],
        pendingConversations: [],
        activeConversations: [],
        endedConversations: [],
        savedConversations: [],
      };
    }
    case conversationsActionTypes.CONVERSATIONS_LOADING_SUCCESS: {
      return {
        ...state,
        allConversations: action.payload.allConversations,
        pendingConversations: action.payload.pendingConversations,
        activeConversations: action.payload.activeConversations,
        endedConversations: action.payload.endedConversations,
        savedConversations: action.payload.savedConversations,
      };
    }
    default: {
      return state;
    }
  }
};
