import { IAuthState, getDefaultAuthState } from '../state/authentication-state';
import { All, authActionType } from '../actions/auth.actions';

export const authReducer = (state: IAuthState = getDefaultAuthState(), action: All): IAuthState => {
  switch (action.type) {
    case authActionType.LOGIN_SUCCESS: {
      return {
        ...state,
        isAuthenticated: true,
        user: {
          access_token: action.payload.access_token,
          email: action.payload.email,
        },
        errorMessage: null,
      };
    }
    case authActionType.LOGIN_FAILURE: {
      return {
        ...state,
        errorMessage: 'Неверный email или пароль!',
      };
    }
    case authActionType.SIGNUP_SUCCESS: {
      return {
        ...state,
        isAuthenticated: false,
        user: {
          access_token: action.payload.access_token,
          email: action.payload.email,
          password: action.payload.password,
        },
        errorMessage: null,
      };
    }
    case authActionType.SIGNUP_FAILURE: {
      return {
        ...state,
        errorMessage: 'Пользователь с данным email уже существует!',
      };
    }
    case authActionType.CHANGEPASS_SUCCESS: {
      return {
        ...state,
        isAuthenticated: false,
        user: {
          access_token: action.payload.access_token,
          password: action.payload.password,
        },
        errorMessage: null,
      };
    }
    case authActionType.CHANGEPASS_FAILURE: {
      return {
        ...state,
        errorMessage: 'Что-то пошло не так!',
      };
    }
    case authActionType.LOGOUT: {
      return getDefaultAuthState();
    }
    default: {
      return state;
    }
  }
};
