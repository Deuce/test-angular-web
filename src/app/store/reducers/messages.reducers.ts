import { IMessagesState, getDefaultMessagesState } from './../state/chat-support-state';
import { All, messagesActionType } from '../actions/messages.actions';

export const sortByDate = (arr: Array<object>, prop: string): Array<object> => {
  return arr.slice().sort((a: object, b: object) => new Date(a[prop]).getTime() - new Date(b[prop]).getTime());
};

export const messagesReducer = (state: IMessagesState = getDefaultMessagesState(), action: All): IMessagesState => {
  switch (action.type) {
    case messagesActionType.MESSAGES_LOADING: {
      return {
        ...state,
        messages: [],
      };
    }
    case messagesActionType.MESSAGES_LOADING_SUCCESS: {
      const data = action.payload.messages;
      const sortedData = sortByDate(data, 'createdAt');

      return {
        ...state,
        messages: sortedData,
      };
    }
    case messagesActionType.ADD_MESSAGE: {
      const data = action.payload.message;

      return {
        ...state,
        messages: [...state.messages, data],
      };
    }
    default: {
      return state;
    }
  }
};
