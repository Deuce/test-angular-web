import { IRemindPassState, getDefaultRemindPassState } from '../state/authentication-state';
import { remindPassActionTypes, All } from '../actions/remind-pass-form.actions';

export const remindPassReducer = (
  state: IRemindPassState = getDefaultRemindPassState(),
  action: All,
): IRemindPassState => {
  switch (action.type) {
    case remindPassActionTypes.REMINDPASS_EMAIL_CHANGED: {
      return {
        ...state,
        email: action.payload.userEmail,
        isEmailDirty: true,
      };
    }
    case remindPassActionTypes.REMINDPASS_SET_VALIDITY: {
      return {
        ...state,
        isValid: action.payload.isValid,
      };
    }
    default: {
      return state;
    }
  }
};
