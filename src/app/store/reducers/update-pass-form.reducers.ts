import { IUpdatePassState, getDefaultUpdatePassState } from '../state/authentication-state';
import { updatePassActionTypes, All } from '../actions/update-pass-form.actions';

export const updatePassReducer = (
  state: IUpdatePassState = getDefaultUpdatePassState(),
  action: All,
): IUpdatePassState => {
  switch (action.type) {
    case updatePassActionTypes.UPDATEPASS_PASSWORD_CHANGE: {
      return {
        ...state,
        password: action.payload.userPassword,
        isPasswordDirty: true,
      };
    }
    case updatePassActionTypes.UPDATEPASS_NEW_PASSWORD_CHANGE: {
      return {
        ...state,
        newPassword: action.payload.userNewPassword,
        isNewPasswordDirty: true,
      };
    }
    case updatePassActionTypes.UPDATEPASS_SET_VALIDITY: {
      return {
        ...state,
        isValid: action.payload.isValid,
      };
    }
    default: {
      return state;
    }
  }
};
