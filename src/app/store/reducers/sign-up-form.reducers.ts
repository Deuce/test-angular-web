import { ISignUpState, getDefaultSignUpState } from '../state/authentication-state';
import { signUpActionTypes, All } from '../actions/sign-up-form.actions';

export const signUpReducer = (state: ISignUpState = getDefaultSignUpState(), action: All): ISignUpState => {
  switch (action.type) {
    case signUpActionTypes.SIGNUP_EMAIL_CHANGED: {
      return {
        ...state,
        email: action.payload.userEmail,
        isEmailDirty: true,
      };
    }
    case signUpActionTypes.SIGNUP_PASSWORD_CHANGED: {
      return {
        ...state,
        password: action.payload.userPassword,
        isPasswordDirty: true,
      };
    }
    case signUpActionTypes.SIGNUP_CONFIRM_PASSWORD_CHANGED: {
      return {
        ...state,
        confirmPassword: action.payload.userConfirmPassword,
        isConfirmPasswordDirty: true,
      };
    }
    case signUpActionTypes.SIGNUP_SET_VALIDITY: {
      return {
        ...state,
        isValid: action.payload.isValid,
      };
    }
    default: {
      return state;
    }
  }
};
