import { ILogInState, getDefaultLogInState } from '../state/authentication-state';
import { logInActionsTypes, All } from '../actions/log-in-form.actions';

export const logInFormReducer = (state: ILogInState = getDefaultLogInState(), action: All): ILogInState => {
  switch (action.type) {
    case logInActionsTypes.LOGIN_EMAIL_CHANGED: {
      return {
        ...state,
        email: action.payload.userEmail,
        isEmailDirty: true,
      };
    }
    case logInActionsTypes.LOGIN_PASSWORD_CHANGED: {
      return {
        ...state,
        password: action.payload.userPassword,
        isPasswordDirty: true,
      };
    }
    case logInActionsTypes.LOGIN_SET_VALIDITY: {
      return {
        ...state,
        isValid: action.payload.isValid,
      };
    }
    default: {
      return state;
    }
  }
};
