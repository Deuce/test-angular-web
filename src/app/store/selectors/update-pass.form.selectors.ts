import { createSelector } from '@ngrx/store';
import { IAuthenticationState, IUpdatePassState } from '../state/authentication-state';

export const selectUpdatePassState = (state: IAuthenticationState): IUpdatePassState => state.update;

export const selectUpdatePassPassword = createSelector(
  selectUpdatePassState,
  (state: IUpdatePassState) => state.password,
);
export const selectUpdatePassNewPassword = createSelector(
  selectUpdatePassState,
  (state: IUpdatePassState) => state.newPassword,
);
export const selectUpdatePassValid = createSelector(selectUpdatePassState, (state: IUpdatePassState) => state.isValid);
export const selectUpdatePassPasswordDirty = createSelector(
  selectUpdatePassState,
  (state: IUpdatePassState) => state.isPasswordDirty,
);
export const selectUpdatePassEmailDirty = createSelector(
  selectUpdatePassState,
  (state: IUpdatePassState) => state.isNewPasswordDirty,
);
