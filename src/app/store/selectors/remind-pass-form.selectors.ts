import { createSelector } from '@ngrx/store';
import { IAuthenticationState, IRemindPassState } from '../state/authentication-state';

export const selectRemindPassState = (state: IAuthenticationState): IRemindPassState => state.remind;

export const selectRemindPassEmail = createSelector(selectRemindPassState, (state: IRemindPassState) => state.email);
export const selectRemindPassValid = createSelector(selectRemindPassState, (state: IRemindPassState) => state.isValid);
export const selectRemindPassEmailDirty = createSelector(
  selectRemindPassState,
  (state: IRemindPassState) => state.isEmailDirty,
);
