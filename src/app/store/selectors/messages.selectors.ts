import { createSelector } from '@ngrx/store';
import { IChatSupportState, IMessagesState } from '../state/chat-support-state';

export const selectMessagesState = (state: IChatSupportState): IMessagesState => state.mess;

export const selectMessages = createSelector(selectMessagesState, (state: IMessagesState) => state.messages);
