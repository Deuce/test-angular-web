import { createSelector } from '@ngrx/store';
import { IChatSupportState, IConversationsState } from '../state/chat-support-state';

export const selectConversationsState = (state: IChatSupportState): IConversationsState => state.convers;

export const selectAllConversations = createSelector(
  selectConversationsState,
  (state: IConversationsState) => state.allConversations,
);
export const selectPendingConversations = createSelector(
  selectConversationsState,
  (state: IConversationsState) => state.pendingConversations,
);
export const selectActiveConversations = createSelector(
  selectConversationsState,
  (state: IConversationsState) => state.activeConversations,
);
export const selectEndedConversations = createSelector(
  selectConversationsState,
  (state: IConversationsState) => state.endedConversations,
);
export const selectSavedConversations = createSelector(
  selectConversationsState,
  (state: IConversationsState) => state.savedConversations,
);
