import { createSelector } from '@ngrx/store';
import { IAuthenticationState, ILogInState } from '../state/authentication-state';

export const selectLogInState = (state: IAuthenticationState): ILogInState => state.login;

export const selectLogInEmail = createSelector(selectLogInState, (state: ILogInState) => state.email);
export const selectLogInPassword = createSelector(selectLogInState, (state: ILogInState) => state.password);
export const selectLogInValid = createSelector(selectLogInState, (state: ILogInState) => state.isValid);
export const selectLogInEmailDirty = createSelector(selectLogInState, (state: ILogInState) => state.isEmailDirty);
export const selectLogInPasswordDirty = createSelector(selectLogInState, (state: ILogInState) => state.isPasswordDirty);
