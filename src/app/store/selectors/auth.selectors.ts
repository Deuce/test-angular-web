import { createSelector } from '@ngrx/store';
import { IAuthenticationState, IAuthState } from '../state/authentication-state';

export const selectAuthState = (state: IAuthenticationState): IAuthState => state.auth;

export const selectAuthUser = createSelector(selectAuthState, (state: IAuthState) => state.user);
export const selectAuthStatus = createSelector(selectAuthState, (state: IAuthState) => state.isAuthenticated);
export const selectAuthMessage = createSelector(selectAuthState, (state: IAuthState) => state.errorMessage);
