import { createSelector } from '@ngrx/store';
import { IAuthenticationState, ISignUpState } from '../state/authentication-state';

export const selectSignUpState = (state: IAuthenticationState): ISignUpState => state.signup;

export const selectSignUpEmail = createSelector(selectSignUpState, (state: ISignUpState) => state.email);
export const selectSignUpPassword = createSelector(selectSignUpState, (state: ISignUpState) => state.password);
export const selectSignUpConfirmPassword = createSelector(
  selectSignUpState,
  (state: ISignUpState) => state.confirmPassword,
);
export const selectSignUpValid = createSelector(selectSignUpState, (state: ISignUpState) => state.isValid);
export const selectSignUpEmailDirty = createSelector(selectSignUpState, (state: ISignUpState) => state.isEmailDirty);
export const selectSignUpPasswordDirty = createSelector(
  selectSignUpState,
  (state: ISignUpState) => state.isPasswordDirty,
);
