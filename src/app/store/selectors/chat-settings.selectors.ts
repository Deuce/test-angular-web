import { createSelector } from '@ngrx/store';
import { IChatSupportState, IPhrasesState } from '../state/chat-support-state';

export const selectPhrasesState = (state: IChatSupportState): IPhrasesState => state.phras;

export const selectPhrases = createSelector(selectPhrasesState, (state: IPhrasesState) => state.phrases);
export const selectGreeting = createSelector(selectPhrasesState, (state: IPhrasesState) => state.greeting);
