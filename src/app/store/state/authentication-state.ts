import { User } from '../../models';

export interface IAuthenticationState {
  login: ILogInState;
  signup: ISignUpState;
  remind: IRemindPassState;
  update: IUpdatePassState;
  auth: IAuthState;
}

export interface ILogInState {
  email: string;
  password: string;
  isValid: boolean;
  isEmailDirty: boolean;
  isPasswordDirty: boolean;
}

export interface ISignUpState {
  email: string;
  password: string;
  confirmPassword: string;
  isValid: boolean;
  isEmailDirty: boolean;
  isPasswordDirty: boolean;
  isConfirmPasswordDirty: boolean;
}

export interface IRemindPassState {
  email: string;
  isValid: boolean;
  isEmailDirty: boolean;
}

export interface IUpdatePassState {
  password: string;
  newPassword: string;
  isValid: boolean;
  isPasswordDirty: boolean;
  isNewPasswordDirty: boolean;
}

export interface IAuthState {
  user: User | '';
  isAuthenticated: boolean;
  errorMessage: string | null;
}

export const getDefaultAuthState = (): IAuthState => {
  return {
    user: '',
    isAuthenticated: false,
    errorMessage: null,
  };
};

export const getDefaultLogInState = (): ILogInState => {
  return {
    email: '',
    password: '',
    isValid: false,
    isEmailDirty: false,
    isPasswordDirty: false,
  };
};

export const getDefaultSignUpState = (): ISignUpState => {
  return {
    email: '',
    password: '',
    confirmPassword: '',
    isValid: false,
    isEmailDirty: false,
    isPasswordDirty: false,
    isConfirmPasswordDirty: false,
  };
};

export const getDefaultRemindPassState = (): IRemindPassState => {
  return {
    email: '',
    isValid: false,
    isEmailDirty: false,
  };
};

export const getDefaultUpdatePassState = (): IUpdatePassState => {
  return {
    password: '',
    newPassword: '',
    isValid: false,
    isPasswordDirty: false,
    isNewPasswordDirty: false,
  };
};
