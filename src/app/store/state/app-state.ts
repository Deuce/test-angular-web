import { IAuthenticationState } from './authentication-state';
import { IChatSupportState } from './chat-support-state';

export interface IAppState {
  authentication: IAuthenticationState;
  chatsupport: IChatSupportState;
}
