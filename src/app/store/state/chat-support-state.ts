export interface IChatSupportState {
  convers: IConversationsState;
  mess: IMessagesState;
  phras: IPhrasesState;
}

export interface IConversationsState {
  allConversations: Array<object>;
  pendingConversations: Array<object>;
  activeConversations: Array<object>;
  endedConversations: Array<object>;
  savedConversations: Array<object>;
}

export interface IMessagesState {
  messages: Array<object>;
}

export interface IPhrasesState {
  phrases: Array<string>;
  greeting: string;
}

export const getDefaultConversationsState = (): IConversationsState => {
  return {
    allConversations: [],
    pendingConversations: [],
    activeConversations: [],
    endedConversations: [],
    savedConversations: [],
  };
};

export const getDefaultMessagesState = (): IMessagesState => {
  return {
    messages: [],
  };
};

export const getDefaultPhrasesState = (): IPhrasesState => {
  return {
    phrases: ['Ожидайте', 'Здравствуйте, измените ваши настройки', 'Пожалуйста, проверьте ваше интернет соединение'],
    greeting: '',
  };
};
