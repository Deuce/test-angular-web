import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, map, exhaustMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { ConversationsService } from '../../services';
import { messagesActionType, Loading, LoadingSuccess } from '../actions/messages.actions';

@Injectable()
export class MessagesEffects {
  conversId: string;

  constructor(private readonly actions: Actions, private readonly conversService: ConversationsService) {}

  @Effect()
  Loading: Observable<unknown> = this.actions.pipe( // tslint:disable-line
    ofType(messagesActionType.MESSAGES_LOADING),
    map((action: Loading) => {
      const href = window.location.toString();
      this.conversId = href.slice(href.lastIndexOf('/') + 1);

      return action.payload;
    }),
    exhaustMap(() =>
      this.conversService.getMessages(this.conversId).pipe(
        map((messages: Array<object>) => new LoadingSuccess({ messages })),
        catchError((error: string) => {
          console.error(error);

          return error;
        }),
      ),
    ),
  );

  @Effect({ dispatch: false })
  LoadingSuccess: Observable<unknown> = this.actions.pipe( // tslint:disable-line
    ofType(messagesActionType.MESSAGES_LOADING_SUCCESS),
    tap((res: object) => {
      console.log(res);
    }),
  );
}
