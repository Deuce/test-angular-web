import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, map, exhaustMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { ConversationsService } from '../../services/conversations.service';
import { conversationsActionTypes, Loading, LoadingSuccess } from '../actions/conversations.actions';

@Injectable()
export class ConversationsEffects {
  constructor(private readonly actions: Actions, private readonly conversService: ConversationsService) {}

  @Effect()
  Loading: Observable<unknown> = this.actions.pipe( // tslint:disable-line
    ofType(conversationsActionTypes.CONVERSATIONS_LOADING),
    map((action: Loading) => action.payload.allConversations),
    exhaustMap(() =>
      this.conversService.getAllConversations().pipe(
        map((convers: Array<object>) => {
          const all: object[] = [];
          const pending: object[] = [];
          const active: object[] = [];
          const ended: object[] = [];
          const saved: object[] = [];

          convers.map((conver: object) => {
            all.push(conver);

            switch (conver['status']) {
              case 'PENDING': {
                pending.push(conver);

                return undefined;
              }
              case 'ACTIVE': {
                active.push(conver);

                return undefined;
              }
              case 'CLOSED': {
                ended.push(conver);

                return undefined;
              }
              case 'SAVED': {
                saved.push(conver);

                return undefined;
              }
              default: {
                return conver;
              }
            }
          });
          const payload = {
            allConversations: [...all],
            pendingConversations: [...pending],
            activeConversations: [...active],
            endedConversations: [...ended],
            savedConversations: [...saved],
          };

          return new LoadingSuccess(payload);
        }),
        catchError((error: string) => {
          console.error(error);

          return error;
        }),
      ),
    ),
  );

  @Effect({ dispatch: false })
  LoadingSuccess: Observable<unknown> = this.actions.pipe( // tslint:disable-line
    ofType(conversationsActionTypes.CONVERSATIONS_LOADING_SUCCESS),
    tap((data: string) => {
      console.log(data);
    }),
  );
}
