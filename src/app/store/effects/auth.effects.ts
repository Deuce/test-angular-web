/* tslint:disable */
import { ToastService } from 'angular-toastify';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, map, exhaustMap, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import {
  authActionType,
  LogIn,
  LogInSuccess,
  LogInFailure,
  SignUp,
  SignUpSuccess,
  SignUpFailure,
  ChangePass,
  ChangePassSuccess,
  ChangePassFailure,
} from '../actions/auth.actions';

@Injectable()
export class AuthEffects {
  constructor(
    private readonly actions: Actions,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly toastService: ToastService,
  ) {}

  // log In

  @Effect()
  LogIn: Observable<unknown> = this.actions.pipe(
    ofType(authActionType.LOGIN),
    map((action: LogIn) => action.payload),
    exhaustMap((action) =>
      this.authService.logIn(action.email, action.password).pipe(
        map((user) => new LogInSuccess({ access_token: user.access_token, email: action.email })),
        catchError((error) => {
          console.log(error);

          return of(new LogInFailure({ error }));
        }),
      ),
    ),
  );

  @Effect({ dispatch: false })
  LogInSuccess: Observable<any> = this.actions.pipe(
    ofType(authActionType.LOGIN_SUCCESS),
    tap((user) => {
      localStorage.setItem('token', user.payload.access_token);
      console.log(user);
      this.router.navigateByUrl('/main');
    }),
  );

  @Effect({ dispatch: false })
  LogInFailure: Observable<any> = this.actions.pipe(ofType(authActionType.LOGIN_FAILURE));

  // Sign Up

  @Effect()
  SignUp: Observable<any> = this.actions.pipe(
    ofType(authActionType.SIGNUP),
    map((action: SignUp) => action.payload),
    exhaustMap((action) =>
      this.authService.signUp(action.email, action.password).pipe(
        map((user) => new SignUpSuccess({ id: user.id, email: action.email, password: action.password })),
        catchError((error: string) => {
          console.log(error);

          return of(new SignUpFailure({ error }));
        }),
      ),
    ),
  );

  @Effect({ dispatch: false })
  SignUpSuccess: Observable<unknown> = this.actions.pipe(
    ofType(authActionType.SIGNUP_SUCCESS),
    tap((user) => {
      console.log(user);
      this.router.navigateByUrl('/');
    }),
  );

  @Effect({ dispatch: false })
  SignUpFailure: Observable<any> = this.actions.pipe(ofType(authActionType.SIGNUP_FAILURE));

  // Change Password

  @Effect()
  ChangePass: Observable<unknown> = this.actions.pipe(
    ofType(authActionType.CHANGEPASS),
    map((action: ChangePass) => action.payload),
    exhaustMap((action) =>
      this.authService.changePass(action.oldPassword, action.newPassword).pipe(
        map((user) => new ChangePassSuccess({ access_token: user.access_token, newPassword: action.newPassword })),
        catchError((error) => {
          console.log(error);

          return of(new ChangePassFailure({ error }));
        }),
      ),
    ),
  );

  @Effect({ dispatch: false })
  ChangePassSuccess: Observable<unknown> = this.actions.pipe(
    ofType(authActionType.CHANGEPASS_SUCCESS),
    tap((user) => {
      console.log(user);
      this.router.navigateByUrl('/');
      this.showSuccesPasswordChange();
    }),
  );

  @Effect({ dispatch: false })
  ChangePassFailure: Observable<unknown> = this.actions.pipe(ofType(authActionType.CHANGEPASS_FAILURE));

  // Log Out

  @Effect({ dispatch: false })
  public LogOut: Observable<unknown> = this.actions.pipe(
    ofType(authActionType.LOGOUT),
    tap((user) => {
      localStorage.removeItem('token');
      localStorage.removeItem('auth');
      this.router.navigateByUrl('/');
    }),
  );

  showSuccesPasswordChange = (): void => {
    this.toastService.info('Ваш пароль был успешно обновлен!');
  };
}
