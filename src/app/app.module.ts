import { CoreModule } from './core/core.module';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastService, AngularToastifyModule } from 'angular-toastify';
import { AuthtenticationModule } from './authentication/authentication.module';
import { ChatSupportModule } from './chat-support/chat-support.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthServiceConfig } from 'angularx-social-login';
import { AuthService, provideConfig, TokenInterceptor, AuthGuardService } from './services';
import { ConversationsService } from './services/conversations.service';
import { SentryErrorHandler } from './services/error.handler.service';
import { routing } from './app-routing.module';

import {
  logInFormReducer,
  signUpReducer,
  remindPassReducer,
  updatePassReducer,
  authReducer,
  conversationsReducer,
  messagesReducer,
  phrasesReducer,
  metaReducers,
} from './store/reducers';

import { AuthEffects, MessagesEffects, ConversationsEffects } from './store/effects';

import * as moment from 'moment';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    CoreModule,
    AngularToastifyModule,
    AuthtenticationModule,
    ChatSupportModule,
    routing,
    EffectsModule.forRoot([AuthEffects, MessagesEffects, ConversationsEffects]),
    StoreModule.forRoot(
      {
        login: logInFormReducer,
        signup: signUpReducer,
        remind: remindPassReducer,
        update: updatePassReducer,
        auth: authReducer,
        convers: conversationsReducer,
        mess: messagesReducer,
        phras: phrasesReducer,
      },
      {
        metaReducers,
        runtimeChecks: {
          strictStateImmutability: true,
          strictActionImmutability: true,
        },
      },
    ),
  ],
  providers: [
    ToastService,
    AuthService,
    ConversationsService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig,
    },
    AuthGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
    // { provide: ErrorHandler, useClass: SentryErrorHandler },
    { provide: 'moment', useFactory: (): object => moment },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
