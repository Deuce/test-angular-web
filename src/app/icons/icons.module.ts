import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeatherModule } from 'angular-feather';
import { Smile } from 'angular-feather/icons';

const icons = {
  Smile,
};

@NgModule({
  declarations: [],
  imports: [FeatherModule.pick(icons), CommonModule],
  exports: [FeatherModule],
})
export class IconsModule {}
