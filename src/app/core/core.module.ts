import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [CommonModule, NgbModule, HttpClientModule, FormsModule, ReactiveFormsModule],
  exports: [NgbModule, HttpClientModule, FormsModule, ReactiveFormsModule],
})
export class CoreModule {}
