import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreModule } from './../core/core.module';
import { routing } from './authentication-routing.module';
import { SocialLoginModule } from 'angularx-social-login';

import {
  LogInContainerComponent,
  LogInComponent,
  SignUpContainerComponent,
  SignUpComponent,
  RemindPasswordContainerComponent,
  RemindPasswordComponent,
  UpdatePasswordContainerComponent,
  UpdatePasswordComponent,
  EmailComponent,
  PasswordComponent,
  NewPasswordComponent,
  ConfirmPasswordComponent,
  GoogleLoginComponent,
  TopNavBarComponent,
} from './components';

@NgModule({
  declarations: [
    LogInContainerComponent,
    LogInComponent,
    SignUpContainerComponent,
    SignUpComponent,
    RemindPasswordContainerComponent,
    RemindPasswordComponent,
    UpdatePasswordContainerComponent,
    UpdatePasswordComponent,
    EmailComponent,
    PasswordComponent,
    NewPasswordComponent,
    ConfirmPasswordComponent,
    GoogleLoginComponent,
    TopNavBarComponent,
  ],
  imports: [CommonModule, CoreModule, routing, SocialLoginModule],
  exports: [TopNavBarComponent],
  bootstrap: [LogInContainerComponent],
})
export class AuthtenticationModule {}
