import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Action, Store, select } from '@ngrx/store';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { distinctUntilChanged, map } from 'rxjs/operators';

import { User } from '../../../models';

import { IUpdatePassState, IAuthenticationState, IAuthState } from '../../../store/state/authentication-state';
import { selectAuthState } from '../../../store/selectors';
import {
  UpdatePassPasswordChanged,
  UpdatePassNewPasswordChanged,
  UpdatePassSetValidity,
} from '../../../store/actions/update-pass-form.actions';
import { ChangePass } from '../../../store/actions/auth.actions';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.sass'],
})
export class UpdatePasswordComponent implements OnInit {
  updatePassForm: FormGroup;
  user: User = new User();
  authState: IAuthState;

  @Input() updatePassState: IUpdatePassState;
  @Output() actionsEmitted: EventEmitter<Array<Action>> = new EventEmitter();

  constructor(private readonly formBuilder: FormBuilder, private readonly store: Store<IAuthenticationState>) {
    const minLengthOfPassword = 8;

    this.updatePassForm = this.formBuilder.group({
      password: new FormControl('', {
        validators: [Validators.required], // tslint:disable-line
      }),
      newPassword: new FormControl('', {
        validators: [
          Validators.required, // tslint:disable-line
          Validators.minLength(minLengthOfPassword), // tslint:disable-line
          Validators.pattern('^((?=.*d)|(?=.*W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$'), // tslint:disable-line
        ],
      }),
    });
  }

  ngOnInit(): void {
    const password = 'password';
    const newPassword = 'newPassword';

    this.updatePassForm.controls[password].valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((userPassword: string) => {
        this.actionsEmitted.emit([new UpdatePassPasswordChanged({ userPassword })]);
      });
    this.updatePassForm.controls[newPassword].valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((userNewPassword: string) => {
        this.actionsEmitted.emit([new UpdatePassNewPasswordChanged({ userNewPassword })]);
      });

    this.updatePassForm.statusChanges
      .pipe(
        distinctUntilChanged(),
        map((status: string) => status === 'VALID'),
      )
      .subscribe((isValid: boolean) => {
        this.actionsEmitted.emit([new UpdatePassSetValidity({ isValid })]);
      });

    this.store.pipe(select(selectAuthState)).subscribe((authState: IAuthState) => {
      this.authState = authState;
    });
  }

  updateUserPassword = (): void => {
    const password = 'password';
    const newPassword = 'newPassword';

    const userData = {
      oldPassword: this.updatePassForm.controls[password].value,
      newPassword: this.updatePassForm.controls[newPassword].value,
    };

    this.store.dispatch(new ChangePass(userData));
  };
}
