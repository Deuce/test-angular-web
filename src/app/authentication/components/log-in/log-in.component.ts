import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { Store, Action, select } from '@ngrx/store';
import { distinctUntilChanged, map } from 'rxjs/operators';

import { User } from '../../../models';

import { IAuthenticationState, ILogInState, IAuthState } from '../../../store/state/authentication-state';
import { selectAuthState } from './../../../store/selectors';

import { LogInEmailChanged, LogInPasswordChanged, LogInSetValidity } from '../../../store/actions/log-in-form.actions';
import { LogIn } from '../../../store/actions/auth.actions';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.sass'],
})
export class LogInComponent implements OnInit {
  logInForm: FormGroup;
  user: User = new User();
  authState: IAuthState;

  @Input() logInState: ILogInState;
  @Output() actionsEmitted: EventEmitter<Action[]> = new EventEmitter();

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly store: Store<IAuthenticationState>,
    private readonly router: Router,
  ) {
    this.logInForm = this.formBuilder.group({
      email: new FormControl('', {
        validators: [Validators.required, Validators.email], // tslint:disable-line
      }),
      password: new FormControl('', {
        validators: [Validators.required], // tslint:disable-line
      }),
    });
  }

  ngOnInit(): void {
    const email = 'email';
    const password = 'password';

    this.logInForm.controls[email].valueChanges.pipe(distinctUntilChanged()).subscribe((userEmail: string) => {
      this.actionsEmitted.emit([new LogInEmailChanged({ userEmail })]);
    });
    this.logInForm.controls[password].valueChanges.pipe(distinctUntilChanged()).subscribe((userPassword: string) => {
      this.actionsEmitted.emit([new LogInPasswordChanged({ userPassword })]);
    });

    this.logInForm.statusChanges
      .pipe(
        distinctUntilChanged(),
        map((status: string) => status === 'VALID'),
      )
      .subscribe((isValid: boolean) => {
        this.actionsEmitted.emit([new LogInSetValidity({ isValid })]);
      });

    this.store.pipe(select(selectAuthState)).subscribe((authState: IAuthState) => {
      this.authState = authState;
    });

    if (this.authState.isAuthenticated) {
      this.router.navigate(['/main']);
    }
  }

  logInToApp = (): void => {
    const email = 'email';
    const password = 'password';

    const userData = {
      email: this.logInForm.controls[email].value,
      password: this.logInForm.controls[password].value,
    };

    this.store.dispatch(new LogIn(userData));
  };
}
