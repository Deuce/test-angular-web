import { Store, StoreModule } from '@ngrx/store';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { LogInComponent } from './log-in.component';
import { AuthService } from './../../../services';

describe('LogInComponent', () => {
  let component: LogInComponent;
  let fixture: ComponentFixture<LogInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, RouterTestingModule, StoreModule.forRoot({})],
      declarations: [LogInComponent],
      providers: [FormBuilder, Store, AuthService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    fixture.autoDetectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('h2').textContent).toEqual('Авторизация');
  });

  it('should contain button', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelectorAll('button').length).toEqual(1);
  });
});
