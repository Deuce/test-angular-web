import { Component } from '@angular/core';

import { AuthService } from 'angularx-social-login';
// import { SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login'; // tslint:disable-line

@Component({
  selector: 'app-google-login',
  templateUrl: './google-login.component.html',
  styleUrls: ['./google-login.component.sass'],
})
export class GoogleLoginComponent {
  // private user: SocialUser;
  // private loggedIn: boolean;

  constructor(private readonly authService: AuthService) {}

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  // ngOnInit(): void {
  //   this.authService.authState.subscribe((user) => {
  //     this.user = user;
  //     this.loggedIn = user != null;
  //   });
  // }

  signOut(): void {
    this.authService.signOut();
  }
}
