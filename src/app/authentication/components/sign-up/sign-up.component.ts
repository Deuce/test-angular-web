import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { Store, Action, select } from '@ngrx/store';
import { distinctUntilChanged, map } from 'rxjs/operators';

import { User } from './../../../models';

import { ISignUpState, IAuthenticationState, IAuthState } from '../../../store/state/authentication-state';
import { selectAuthState } from '../../../store/selectors';
import {
  SignUpEmailChanged,
  SignUpPasswordChanged,
  SignUpConfirmPasswordChanged,
  SignUpSetValidity,
} from '../../../store/actions/sign-up-form.actions';
import { SignUp } from '../../../store/actions/auth.actions';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.sass'],
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;
  user: User = new User();
  authState: IAuthState;

  @Input() signUpState: ISignUpState;
  @Output() actionsEmitted: EventEmitter<Action[]> = new EventEmitter();

  constructor(private readonly formBuilder: FormBuilder, private readonly store: Store<IAuthenticationState>) {
    const minLengthOfPass = 8;
    this.signUpForm = this.formBuilder.group(
      {
        email: new FormControl('', {
          validators: [Validators.required, Validators.email], // tslint:disable-line
        }),
        password: new FormControl('', {
          validators: [
            Validators.required, // tslint:disable-line
            Validators.minLength(minLengthOfPass), // tslint:disable-line
            Validators.pattern('^((?=.*d)|(?=.*W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$'), // tslint:disable-line
          ],
        }),
        confirmPassword: new FormControl('', {
          validators: [
            Validators.required, // tslint:disable-line
            Validators.minLength(minLengthOfPass), // tslint:disable-line
            Validators.pattern('^((?=.*d)|(?=.*W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$'), // tslint:disable-line
          ],
        }),
      },
      {
        validators: this.mustMatch('password', 'confirmPassword'),
      },
    );
  }

  ngOnInit(): void {
    const email = 'email';
    const password = 'password';
    const confirmPassword = 'confirmPassword';

    this.signUpForm.controls[email].valueChanges.pipe(distinctUntilChanged()).subscribe((userEmail: string) => {
      this.actionsEmitted.emit([new SignUpEmailChanged({ userEmail })]);
    });
    this.signUpForm.controls[password].valueChanges.pipe(distinctUntilChanged()).subscribe((userPassword: string) => {
      this.actionsEmitted.emit([new SignUpPasswordChanged({ userPassword })]);
    });
    this.signUpForm.controls[confirmPassword].valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((userConfirmPassword: string) => {
        this.actionsEmitted.emit([new SignUpConfirmPasswordChanged({ userConfirmPassword })]);
      });

    this.signUpForm.statusChanges
      .pipe(
        distinctUntilChanged(),
        map((status: string) => status === 'VALID'),
      )
      .subscribe((isValid: boolean) => {
        this.actionsEmitted.emit([new SignUpSetValidity({ isValid })]);
      });

    this.store.pipe(select(selectAuthState)).subscribe((authState: IAuthState) => {
      this.authState = authState;
    });
  }

  mustMatch = (controlName: string, matchingControlName: string): object => (formGroup: FormGroup): void => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) { // tslint:disable-line
      return;
    }

    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };

  signUpToApp = (): void => {
    const email = 'email';
    const password = 'password';

    const userData = {
      email: this.signUpForm.controls[email].value,
      password: this.signUpForm.controls[password].value,
    };

    this.store.dispatch(new SignUp(userData));
  };
}
