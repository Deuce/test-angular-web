import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-confirm-password',
  templateUrl: './confirm-password.component.html',
  styleUrls: ['./confirm-password.component.sass'],
})
export class ConfirmPasswordComponent {
  @Input() form: FormGroup;
  @Input() isConfirmPasswordDirty: boolean;

  //  ngOnInit(): void {}
}
