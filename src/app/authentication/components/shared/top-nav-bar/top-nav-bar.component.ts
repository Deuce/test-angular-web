import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAuthenticationState, IAuthState } from '../../../../store/state/authentication-state';
import { selectAuthState } from '../../../../store/selectors';
import { LogOut } from '../../../../store/actions/auth.actions';

@Component({
  selector: 'app-top-nav-bar',
  templateUrl: './top-nav-bar.component.html',
  styleUrls: ['./top-nav-bar.component.sass'],
})
export class TopNavBarComponent implements OnInit {
  userForm: FormGroup;

  email: string;
  username: string;

  constructor(
    private readonly store: Store<IAuthenticationState>,
    private readonly formBuilder: FormBuilder,
    private readonly modalService: NgbModal,
  ) {
    this.userForm = this.formBuilder.group({
      name: new FormControl('', {}),
    });
  }

  logOut = (): void => {
    this.store.dispatch(new LogOut());
  };

  ngOnInit(): void {
    this.username = localStorage.getItem('username');
    this.userForm.controls.name.setValue(this.username);

    this.store.pipe(select(selectAuthState)).subscribe((authState: IAuthState) => {
      this.email = authState.user['email'];
    });
  }

  openConfigurationProfileModal = (content: object): void => {
    this.modalService.open(content, { size: 'lg' });
  };

  changeName = (): void => {
    const username = this.userForm.value.name;
    localStorage.setItem('username', username);
  };
}
