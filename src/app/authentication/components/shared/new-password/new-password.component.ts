import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.sass'],
})
export class NewPasswordComponent {
  @Input() form: FormGroup;
  @Input() isNewPasswordDirty: boolean;

  //  ngOnInit(): void {}
}
