import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.sass'],
})
export class PasswordComponent {
  @Input() form: FormGroup;
  @Input() isPasswordDirty: boolean;

  //  ngOnInit(): void {}
}
