import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.sass'],
})
export class EmailComponent {
  @Input() form: FormGroup;
  @Input() isEmailDirty: boolean;

  //  ngOnInit(): void {}
}
