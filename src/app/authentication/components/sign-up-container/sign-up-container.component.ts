import { Component } from '@angular/core';
import { Action, Store, select } from '@ngrx/store';

import { IAuthenticationState, ISignUpState } from '../../../store/state/authentication-state';
import { selectSignUpState } from '../../../store/selectors';

@Component({
  selector: 'app-sign-up-container',
  templateUrl: './sign-up-container.component.html',
  styleUrls: ['./sign-up-container.component.sass'],
})
export class SignUpContainerComponent {
  public signUpState: ISignUpState;

  constructor(private readonly store: Store<IAuthenticationState>) {
    this.store.pipe(select(selectSignUpState)).subscribe((signUpState: ISignUpState) => {
      this.signUpState = signUpState;
    });
  }

  onSignUpActions($event: Action[]): void {
    const actions = $event;
    actions.forEach(this.store.dispatch.bind(this.store));
  }
}
