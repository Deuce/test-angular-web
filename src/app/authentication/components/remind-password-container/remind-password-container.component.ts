import { Component } from '@angular/core';
import { Action, Store, select } from '@ngrx/store';

import { IAuthenticationState, IRemindPassState } from '../../../store/state/authentication-state';
import { selectRemindPassState } from '../../../store/selectors';

@Component({
  selector: 'app-remind-password-container',
  templateUrl: './remind-password-container.component.html',
  styleUrls: ['./remind-password-container.component.sass'],
})
export class RemindPasswordContainerComponent {
  public remindPassState: IRemindPassState;

  constructor(private readonly store: Store<IAuthenticationState>) {
    this.store.pipe(select(selectRemindPassState)).subscribe((remindPassState: IRemindPassState) => {
      this.remindPassState = remindPassState;
    });
  }

  onRemindPassActions($event: Array<Action>): void {
    const actions = $event;
    actions.forEach(this.store.dispatch.bind(this.store));
  }
}
