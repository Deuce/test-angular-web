import { Component } from '@angular/core';
import { Action, Store, select } from '@ngrx/store';

import { IAuthenticationState, IUpdatePassState } from '../../../store/state/authentication-state';
import { selectUpdatePassState } from '../../../store/selectors';

@Component({
  selector: 'app-update-password-container',
  templateUrl: './update-password-container.component.html',
  styleUrls: ['./update-password-container.component.sass'],
})
export class UpdatePasswordContainerComponent {
  public updatePassState: IUpdatePassState;

  constructor(private readonly store: Store<IAuthenticationState>) {
    this.store.pipe(select(selectUpdatePassState)).subscribe((updatePassState: IUpdatePassState) => {
      this.updatePassState = updatePassState;
    });
  }

  onUpdatePassActions($event: Array<Action>): void {
    const actions = $event;
    actions.forEach(this.store.dispatch.bind(this.store));
  }
}
