import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { ToastService } from 'angular-toastify';

import { RemindPassEmailChanged, RemindPassSetValidity } from '../../../store/actions/remind-pass-form.actions';
import { IRemindPassState, IAuthenticationState } from '../../../store/state/authentication-state';

@Component({
  selector: 'app-remind-password',
  templateUrl: './remind-password.component.html',
  styleUrls: ['./remind-password.component.sass'],
})
export class RemindPasswordComponent implements OnInit {
  remindPassForm: FormGroup;

  @Input() remindPassState: IRemindPassState;
  @Output() actionsEmitted: EventEmitter<Array<Action>> = new EventEmitter();

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly toastService: ToastService,
    private readonly store: Store<IAuthenticationState>,
  ) {
    this.remindPassForm = this.formBuilder.group({
      email: new FormControl('', {
        validators: [Validators.required, Validators.email], // tslint:disable-line
      }),
    });
  }

  ngOnInit(): void {
    const email = 'email';

    this.remindPassForm.controls[email].valueChanges.pipe(distinctUntilChanged()).subscribe((userEmail: string) => {
      this.actionsEmitted.emit([new RemindPassEmailChanged({ userEmail })]);
    });

    this.remindPassForm.statusChanges
      .pipe(
        distinctUntilChanged(),
        map((status: string) => status === 'VALID'),
      )
      .subscribe((isValid: boolean) => {
        this.actionsEmitted.emit([new RemindPassSetValidity({ isValid })]);
      });
  }

  showRemindPasswordMessage = (): void => {
    this.toastService.info('На ваш почтовый ящик был выслан пароль');
  };

  sendUserEmail = (): void => {
    const email = 'email';

    const userData = {
      email: this.remindPassForm.controls[email].value,
    };

    this.showRemindPasswordMessage();
  };
}
