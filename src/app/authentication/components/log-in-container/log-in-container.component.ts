import { Component } from '@angular/core';
import { Action, Store, select } from '@ngrx/store';

import { IAuthenticationState, ILogInState } from '../../../store/state/authentication-state';
import { selectLogInState } from '../../../store/selectors';

@Component({
  selector: 'app-log-in-container',
  templateUrl: './log-in-container.component.html',
  styleUrls: ['./log-in-container.component.sass'],
})
export class LogInContainerComponent {
  public logInState: ILogInState;

  constructor(private readonly store: Store<IAuthenticationState>) {
    this.store.pipe(select(selectLogInState)).subscribe((logInState: ILogInState) => {
      this.logInState = logInState;
    });
  }

  onLogInActions($event: Action[]): void {
    const actions = $event;
    actions.forEach(this.store.dispatch.bind(this.store));
  }
}
