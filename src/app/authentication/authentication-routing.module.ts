import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import {
  LogInContainerComponent,
  SignUpContainerComponent,
  RemindPasswordContainerComponent,
  UpdatePasswordContainerComponent,
} from './components';

import { AuthGuardService } from './../services';

export const routes: Routes = [
  { path: 'log-in', component: LogInContainerComponent },
  { path: 'sign-up', component: SignUpContainerComponent },
  { path: 'remind-password', component: RemindPasswordContainerComponent },
  { path: 'update-password', component: UpdatePasswordContainerComponent, canActivate: [AuthGuardService] },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
