/* tslint:disable */
import * as Sentry from '@sentry/browser';
import { ErrorHandler, Injectable } from '@angular/core';

Sentry.init({
  dsn: 'https://3dbfa8014500460b8354424bc9b31c39@sentry.io/5186009',
});

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  handleError(error) {
    const eventId = Sentry.captureException(error.originalError || error);
    Sentry.showReportDialog({ eventId });
  }
}
