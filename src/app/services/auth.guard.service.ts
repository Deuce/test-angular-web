import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private readonly router: Router, private readonly authService: AuthService) {}

  canActivate(): boolean {
    const isLoggedIn = this.authService.getAutheticatedStatus();

    if (isLoggedIn) {
      return true;
    }
    this.router.navigate(['/']);

    return false;
  }
}
