import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { User } from '../models';

import { IAuthenticationState, IAuthState } from '../store/state/authentication-state';
import { selectAuthState } from '../store/selectors';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user: User;
  authState: IAuthState;

  private readonly BASE_URL = 'http://142.93.103.89';
  constructor(private readonly http: HttpClient, private readonly store: Store<IAuthenticationState>) {
    this.store.pipe(select(selectAuthState)).subscribe((authState: IAuthState) => {
      this.authState = authState;
    });
  }

  getTokenFromLocalStorage = (): string => {
    return localStorage.getItem('token');
  };

  getTokenFromState = (): string => {
    return this.authState.user['access_token'];
  };

  getAutheticatedStatus = (): boolean => {
    return this.authState.isAuthenticated;
  };

  logIn = (email: string, password: string): Observable<User> => {
    const url = `${this.BASE_URL}/login`;

    return this.http.post<User>(url, { email, password });
  };

  signUp = (email: string, password: string): Observable<User> => {
    const url = `${this.BASE_URL}/register`;

    return this.http.post<User>(url, { email, password });
  };

  changePass = (oldPassword: string, newPassword: string): Observable<User> => {
    const url = `${this.BASE_URL}/change-password`;

    return this.http.patch<User>(url, { oldPassword, newPassword });
  };

  getProfile = (): Observable<User> => {
    const url = `${this.BASE_URL}/profile`;

    return this.http.get<User>(url, {});
  };
}
