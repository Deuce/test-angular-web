import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from './../models';

@Injectable({
  providedIn: 'root',
})
export class ConversationsService {
  private readonly BASE_URL = 'http://142.93.103.89';

  constructor(private readonly http: HttpClient) {}

  // conversations

  createNewConversation = (): Observable<object> => {
    const url = `${this.BASE_URL}/conversations`;

    return this.http.post<User>(url, {});
  };

  getAllConversations = (): Observable<any> => { // tslint:disable-line
    const url = `${this.BASE_URL}/conversations`;

    return this.http.get<User>(url, {});
  };

  getConversationByID = (id: string): Observable<any> => { // tslint:disable-line
    const url = `${this.BASE_URL}/conversations/${id}`;

    return this.http.get<User>(url, {});
  };

  changeConversationStatusById = (id: string, body: object): Observable<User> => {
    const url = `${this.BASE_URL}/conversations/${id}`;

    return this.http.patch<User>(url, body);
  };

  deleteConversationById = (id: string): Observable<any> => { // tslint:disable-line
    const url = `${this.BASE_URL}/conversations/${id}`;

    return this.http.delete<User>(url, {});
  };

  // messages

  createNewMessage = (id: string, body: string): Observable<object> => {
    const url = `${this.BASE_URL}/conversations/${id}/messages`;

    return this.http.post<User>(url, { body });
  };

  getMessages = (id: string): Observable<object> => {
    const url = `${this.BASE_URL}/conversations/${id}/messages`;

    return this.http.get<User>(url, {});
  };
}
