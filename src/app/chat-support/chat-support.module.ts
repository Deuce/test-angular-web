import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from './../core/core.module';
import { routing } from './chat-support-routing.module';

import { IconsModule } from './../icons/icons.module';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { EmojiModule } from '@ctrl/ngx-emoji-mart/ngx-emoji';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import {
  ActiveConversationsComponent,
  ActiveListConversationsComponent,
  ConversationComponent,
  ConversationSettingsComponent,
  EndedConversationsComponent,
  EndedListConversationsComponent,
  MainPageComponent,
  MessagesComponent,
  PendingConversationsComponent,
  PendingListConversationsComponent,
  SavedConversationsComponent,
  SavedListConversationsComponent,
  ConversationsMenuComponent,
} from './components';

import { AuthtenticationModule } from '../authentication/authentication.module';

@NgModule({
  declarations: [
    ActiveConversationsComponent,
    ActiveListConversationsComponent,
    ConversationComponent,
    ConversationSettingsComponent,
    EndedConversationsComponent,
    EndedListConversationsComponent,
    MainPageComponent,
    MessagesComponent,
    PendingConversationsComponent,
    PendingListConversationsComponent,
    SavedConversationsComponent,
    SavedListConversationsComponent,
    ConversationsMenuComponent,
  ],
  imports: [
    CommonModule,
    CoreModule,
    routing,
    AuthtenticationModule,
    PickerModule,
    EmojiModule,
    IconsModule,
    Ng2SearchPipeModule,
  ],
  bootstrap: [MainPageComponent],
})
export class ChatSupportModule {}
