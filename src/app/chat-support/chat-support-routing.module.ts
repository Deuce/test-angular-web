import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import {
  MainPageComponent,
  PendingConversationsComponent,
  ActiveConversationsComponent,
  EndedConversationsComponent,
  SavedConversationsComponent,
  ConversationComponent,
} from './components';

import { AuthGuardService } from '../services';

export const routes: Routes = [
  { path: 'main', component: MainPageComponent },
  { path: 'pending-conversations', component: PendingConversationsComponent, canActivate: [AuthGuardService] },
  { path: 'active-conversations', component: ActiveConversationsComponent, canActivate: [AuthGuardService] },
  { path: 'ended-conversations', component: EndedConversationsComponent, canActivate: [AuthGuardService] },
  { path: 'saved-conversations', component: SavedConversationsComponent, canActivate: [AuthGuardService] },
  { path: 'conversation/:conversationId', component: ConversationComponent, canActivate: [AuthGuardService] },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
