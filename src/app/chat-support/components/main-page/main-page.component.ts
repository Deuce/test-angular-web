import { Component, AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { IAuthenticationState, IAuthState } from '../../../store/state/authentication-state';
import { selectAuthState } from './../../../store/selectors';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.sass'],
})
export class MainPageComponent implements AfterContentInit {
  isAuthenticated: boolean;
  getState: Observable<object>;

  constructor(private readonly store: Store<IAuthenticationState>, private readonly router: Router) {
    this.getState = this.store.select(selectAuthState);
  }

  ngAfterContentInit(): void {
    this.getState.subscribe((state: IAuthState) => {
      this.isAuthenticated = state.isAuthenticated;
    });

    if (!this.isAuthenticated) {
      this.router.navigate(['/']);
    }
  }
}
