import { Component, OnInit, Inject } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';

import { Loading } from '../../../store/actions/conversations.actions';

import { IChatSupportState, IConversationsState } from '../../../store/state/chat-support-state';
import { selectConversationsState } from '../../../store/selectors';
import { ConversationsService } from '../../../services';

@Component({
  selector: 'app-active-list-conversations',
  templateUrl: './active-list-conversations.component.html',
  styleUrls: ['./active-list-conversations.component.sass'],
})
export class ActiveListConversationsComponent implements OnInit {
  activeConversations: Array<object>;
  term: string;

  constructor(
    private readonly store: Store<IChatSupportState>,
    private readonly conversService: ConversationsService,
    private readonly router: Router,
    @Inject('moment') public moment, // tslint:disable-line
  ) {}

  ngOnInit(): void {
    this.store.dispatch(new Loading({}));

    this.store.pipe(select(selectConversationsState)).subscribe((conversationsState: IConversationsState) => {
      this.activeConversations = conversationsState.activeConversations;
    });
  }

  saveConversation = (id: string): void => {
    const body = {
      status: 'SAVED',
    };

    this.conversService.changeConversationStatusById(id, body).subscribe();
    this.store.dispatch(new Loading({}));
  };

  enterToConversation = (id: string): void => {
    this.router.navigateByUrl(`/conversation/${id}`);
  };
}
