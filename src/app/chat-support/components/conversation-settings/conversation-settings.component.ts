import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Store, select } from '@ngrx/store';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';

import { IChatSupportState, IPhrasesState } from '../../../store/state/chat-support-state';
import { selectPhrasesState } from '../../../store/selectors';
import { AddPhrase, DeletePhrase, ChangeGreeting } from '../../../store/actions/chat-settings.actions';

@Component({
  selector: 'app-conversation-settings',
  templateUrl: './conversation-settings.component.html',
  styleUrls: ['./conversation-settings.component.sass'],
})
export class ConversationSettingsComponent implements OnInit {
  @Input() phrases: Array<object>;
  conversetionSettingsForm: FormGroup;
  greeting: string;

  constructor(
    private readonly modalService: NgbModal,
    private readonly store: Store<IChatSupportState>,
    private readonly formBuilder: FormBuilder,
  ) {
    this.conversetionSettingsForm = this.formBuilder.group({
      phrase: new FormControl('', {
        validators: [Validators.required], // tslint:disable-line
      }),
      greeting: new FormControl('', {}),
    });
  }

  ngOnInit(): void {
    this.store.pipe(select(selectPhrasesState)).subscribe((phrasesState: IPhrasesState) => {
      this.greeting = phrasesState.greeting;
    });

    this.conversetionSettingsForm.controls.greeting.setValue(this.greeting);
  }

  openConversationSettings = (content: object): void => {
    this.modalService.open(content, { size: 'xl' });
  };

  addNewPhrase = (): void => {
    const payload = { phrase: this.conversetionSettingsForm.value.phrase };
    this.store.dispatch(new AddPhrase(payload));
    this.conversetionSettingsForm.reset();
  };

  deletePhrase = (value: string): void => {
    this.store.dispatch(new DeletePhrase(value));
  };

  saveGreeting = (): void => {
    const payload = { greeting: this.conversetionSettingsForm.value.greeting };
    this.store.dispatch(new ChangeGreeting(payload));
  };
}
