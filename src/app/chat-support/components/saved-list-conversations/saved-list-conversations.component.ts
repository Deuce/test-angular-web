import { Router } from '@angular/router';
import { Component, OnInit, Inject } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Loading } from 'src/app/store/actions/conversations.actions';
import { IChatSupportState, IConversationsState } from '../../../store/state/chat-support-state';
import { ConversationsService } from './../../../services/conversations.service';

import { selectConversationsState } from '../../../store/selectors';

@Component({
  selector: 'app-saved-list-conversations',
  templateUrl: './saved-list-conversations.component.html',
  styleUrls: ['./saved-list-conversations.component.sass'],
})
export class SavedListConversationsComponent implements OnInit {
  getState: Observable<object>;
  savedConversations: Array<object>;
  term: string;

  constructor(
    private readonly store: Store<IChatSupportState>,
    private readonly conversService: ConversationsService,
    private readonly router: Router,
    @Inject('moment') public moment, // tslint:disable-line
  ) {}

  ngOnInit(): void {
    this.store.dispatch(new Loading({}));

    this.store.pipe(select(selectConversationsState)).subscribe((conversationsState: IConversationsState) => {
      this.savedConversations = conversationsState.savedConversations;
      console.log(this.savedConversations);
    });
  }

  deleteConversation(id: string): void {
    this.conversService.deleteConversationById(id).subscribe();
    this.store.dispatch(new Loading({}));
  }

  enterToConversation = (id: string): void => {
    this.router.navigateByUrl(`/conversation/${id}`);
  };
}
