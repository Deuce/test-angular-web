import { Component, OnInit, Inject } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { IChatSupportState, IMessagesState, IPhrasesState } from '../../../store/state/chat-support-state';
import { selectMessagesState, selectPhrasesState } from '../../../store/selectors';
import { ConversationsService } from './../../../services';

import { AddMessage } from '../../../store/actions/messages.actions';
import { Loading } from 'src/app/store/actions/messages.actions';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass'],
})
export class MessagesComponent implements OnInit {
  messageForm: FormGroup;
  rating: FormControl;

  messages: Array<object>;
  phrases: Array<string>;
  term: string;

  conversId: string;
  conversStatus: string;
  conversUpdatedAt: string;

  showEmojiPicker = false;

  constructor(
    private readonly store: Store<IChatSupportState>,
    private readonly conversService: ConversationsService,
    private readonly route: ActivatedRoute,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    @Inject('moment') public moment, // tslint:disable-line
  ) {
    this.messageForm = this.formBuilder.group({
      message: new FormControl('', {}),
      patternMessage: new FormControl('', {}),
    });

    this.rating = new FormControl(null, Validators.required);
  }

  ngOnInit(): void {
    this.store.dispatch(new Loading({}));
    this.conversId = this.route.snapshot.params.conversationId;

    // setInterval(() => this.store.dispatch(new Loading({})), 10000);

    this.conversService.getConversationByID(this.conversId).subscribe((convers) => {

      this.conversStatus = convers.status;
      this.conversUpdatedAt = convers.updatedAt;
    });

    this.store.pipe(select(selectMessagesState)).subscribe((messagesState: IMessagesState) => {
      this.messages = messagesState.messages;
    });

    this.store.pipe(select(selectPhrasesState)).subscribe((phrasesState: IPhrasesState) => {
      this.phrases = phrasesState.phrases;
    });
  }

  addMessage = (id: string): void => {
    const userMessage = this.messageForm.value.message;
    const patternMessage = this.messageForm.value.patternMessage;

    if (!patternMessage) {
      this.conversService.createNewMessage(id, userMessage).subscribe();
      this.store.dispatch(new AddMessage(userMessage));
    } else {
      this.conversService.createNewMessage(id, patternMessage).subscribe();
      this.store.dispatch(new AddMessage(patternMessage));
    }

    this.store.dispatch(new Loading({}));
    this.messageForm.reset();
  };

  closeConversation = (id: string): void => {
    const body = {
      status: 'CLOSED',
    };
    this.conversService.changeConversationStatusById(id, body).subscribe();
    this.router.navigateByUrl('/main');
  };

  toggleEmojiPicker = (): void => {
    this.showEmojiPicker = !this.showEmojiPicker;
  };

  addEmoji = (event: object): void => {
    const message = this.messageForm.controls.message.value;
    const newMessage = `${message}${event['emoji'].native}`;

    this.messageForm.controls.message.setValue(newMessage);
    this.showEmojiPicker = false;
  };

  ratingConversation = ():void => {
    const rate = this.rating.value;
    // to server
    if (rate) {
      this.router.navigateByUrl('main');
    }
  };

  search = (text$: Observable<string>): Observable<string[]> => {
    const period = 200;
    const minLength = 2;

    return text$.pipe(
      debounceTime(period),
      distinctUntilChanged(),
      map(
        (term: string) =>
          term.length < minLength
            ? []
            : this.phrases.filter((v: string) => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10), // tslint:disable-line
      ),
    );
  };
}
