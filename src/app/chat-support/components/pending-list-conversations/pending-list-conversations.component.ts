import { Component, OnInit, Inject } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';

import { Loading } from 'src/app/store/actions/conversations.actions';
import { IChatSupportState, IConversationsState, IPhrasesState } from '../../../store/state/chat-support-state';
import { ConversationsService } from './../../../services/conversations.service';
import { AddMessage } from '../../../store/actions/messages.actions';

import { selectConversationsState, selectPhrasesState } from '../../../store/selectors';

@Component({
  selector: 'app-pending-list-conversations',
  templateUrl: './pending-list-conversations.component.html',
  styleUrls: ['./pending-list-conversations.component.sass'],
})
export class PendingListConversationsComponent implements OnInit {
  pendingConversations: Array<object>;
  greeting = '';
  term: string;

  constructor(
    private readonly store: Store<IChatSupportState>,
    private readonly conversService: ConversationsService,
    private readonly router: Router,
    @Inject('moment') public moment: () => string,
  ) {}

  ngOnInit(): void {
    this.store.dispatch(new Loading({}));

    this.store.pipe(select(selectConversationsState)).subscribe((conversationsState: IConversationsState) => {
      this.pendingConversations = conversationsState.pendingConversations;
    });

    this.store.pipe(select(selectPhrasesState)).subscribe((phrasesState: IPhrasesState) => {
      this.greeting = phrasesState.greeting;
    });
  }

  createConversation = (): void => {
    this.conversService.createNewConversation().subscribe();
    this.store.dispatch(new Loading({}));
  };

  enterToConversation = (id: string): void => {
    const body = {
      status: 'ACTIVE',
    };
    this.conversService.changeConversationStatusById(id, body).subscribe();
    this.router.navigateByUrl(`/conversation/${id}`);

    if (this.greeting !== '') {
      const message = {
        body: this.greeting,
      };

      this.conversService.createNewMessage(id, this.greeting).subscribe();
      this.store.dispatch(new AddMessage({ message }));
    }
  };
}
