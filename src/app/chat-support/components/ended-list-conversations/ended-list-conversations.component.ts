import { Component, OnInit, Inject } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';

import { Loading } from 'src/app/store/actions/conversations.actions';

import { IChatSupportState, IConversationsState } from '../../../store/state/chat-support-state';
import { selectConversationsState } from '../../../store/selectors';
import { ConversationsService } from './../../../services';

@Component({
  selector: 'app-ended-list-conversations',
  templateUrl: './ended-list-conversations.component.html',
  styleUrls: ['./ended-list-conversations.component.sass'],
})
export class EndedListConversationsComponent implements OnInit {
  endedConversations: Array<object>;
  term: string;

  constructor(
    private readonly store: Store<IChatSupportState>,
    private readonly conversService: ConversationsService,
    private readonly router: Router,
    @Inject('moment') public moment, // tslint:disable-line
  ) {}

  ngOnInit(): void {
    this.store.dispatch(new Loading({}));

    this.store.pipe(select(selectConversationsState)).subscribe((conversationsState: IConversationsState) => {
      this.endedConversations = conversationsState.endedConversations;
    });
  }

  saveConversation = (id: string): void => {
    const body = {
      status: 'SAVED',
    };

    this.conversService.changeConversationStatusById(id, body).subscribe();
    this.store.dispatch(new Loading({}));
  };

  enterToConversation = (id: string): void => {
    this.router.navigateByUrl(`/conversation/${id}`);
  };
}
