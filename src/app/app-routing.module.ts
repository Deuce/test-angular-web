import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'log-in' },
  { path: 'log-in', loadChildren: './authentication/authentication.module#AuthenticationModule' },
  { path: 'main', loadChildren: './chat-support/chat-support.module#ChatSupportModule' },
  { path: '**', redirectTo: '/' },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
